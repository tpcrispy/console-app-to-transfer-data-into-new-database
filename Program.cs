﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace updatedatabases
{
    class Program
    {
        static void Main(string[] args)
        {
            // THIS IS WHERE WE COPY THE DATA FROM
            string clientDataBase = @"Server=127.0.0.1,1433;User Id=SA;Password=reallyStrongPwd123";
            // THIS IS WHERE THE DATA ENDS UP
			string endDataBase = @"Server=127.0.0.1,2019;User Id=SA;Password=reallyStrongPwd123";

			using (SqlConnection ClientDataBaseConnection = new SqlConnection(clientDataBase))
			{
                // OPEN THE CONNENTION TO THE SOURCE/CLIENT DATABASE
                ClientDataBaseConnection.Open();

                // GETS ALL THE TABLES FROM SOURCE DATABASE
                DataTable dt = ClientDataBaseConnection.GetSchema("Tables");
				Console.WriteLine($"{dt.Rows.Count} TABLES FOUND!");

                // LOOP THROUGH THE TABLES FOUND 
                foreach(DataRow dataRow in dt.Rows)
                {
					// DONT WANT TO TRY AND COPY A VIEW
                    if ($"{dataRow[3]}".Equals("VIEW"))
                    {
					    Console.WriteLine($"{dataRow[3]}");
					    continue;
                    }

					// dataRow[2] == TABLE NAME EG: SELECT * FROM TABLE NAME in dataRow 
					SqlCommand headerData = new SqlCommand($"SELECT * FROM {(string)dataRow[2]}", ClientDataBaseConnection);
                    SqlDataReader readerHeader = headerData.ExecuteReader();

					// NEW CONNECTION TO THE ENDDATABASE SO WE CAN COPY THE DATA INTO IT
					using (SqlConnection EndDataBaseConnection = new SqlConnection(endDataBase))
					{ 
                        EndDataBaseConnection.Open();
                        
                        // DELETE THE TABLE IN THE END DATABASE (SO WE CAN REPOPULATE IT)
                        SqlCommand deleteHeader = new SqlCommand($"DELETE FROM {(string)dataRow[2]}", EndDataBaseConnection);
                        var numberOfDeletedRows = deleteHeader.ExecuteNonQuery();
						Console.WriteLine($"Number of deleted rows: {numberOfDeletedRows}");

						// Create the SqlBulkCopy object (USED TO DO BULK DATA TRANSFER). 
						using (SqlBulkCopy bulkCopy = new SqlBulkCopy(endDataBase))
						{
						    Console.WriteLine("STARTING TO COPY DATABASE...");
						    bulkCopy.DestinationTableName = (string)dataRow[2];
                            try
                            {
                                bulkCopy.WriteToServer(readerHeader);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                            finally
                            {
                            readerHeader.Close();
                            }
						}
					}
				}
			}
        }
    }
}
