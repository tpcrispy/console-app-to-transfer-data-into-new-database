# READ ME

Simple project to copy a client/source databse into a pre-existing (end) database.

## Assumptions

- The Source and Target database configuration and structure are equivalent.
- The Target database’s tables are already created (TODO - add create new table).
- User must edit the database connection strings within the code (ie. hardcoded, can’t be passed as a command value).
- The Source and Target databases are not in read/write use whilst this application is running happens.

## Dependencies

(can just do a dotnet restore)

- System.Data.SqlClient --version 4.8.2

```
dotnet add package System.Data.SqlClient --version 4.8.2
```

## local setup

docker for mysql server

```
sudo docker pull mcr.microsoft.com/mssql/server:2019-latest
```

Client Database - (port 1433)

```
docker run -d --name sql_server_demo2 -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=reallyStrongPwd123' -p 1433:1433 mcr.microsoft.com/mssql/server:2019-latest
```

END Database - (port 2019)

```
docker run -d --name sql_server_demo2 -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=reallyStrongPwd123' -p 2019:1433 mcr.microsoft.com/mssql/server:2019-latest
```

Simple create/insert to test some data (very basic)

```
CREATE TABLE Persons ( PersonID int, LastName varchar(255), FirstName varchar(255), Address varchar(255), City varchar(255));

INSERT INTO Persons (PersonID, LastName, FirstName, Address, City) VALUES (1, 'Tom', 'Smith', 'blah street', 'hobart');
```

## Basic Summary

The console app queries the client database to grab all tables

```
DataTable dt = ClientDataBaseConnection.GetSchema("Tables");
```

it then loops through each table (checking to make sure it's not a view table)

```
foreach(DataRow dataRow in dt.Rows)
{
    // DONT WANT TO TRY AND COPY A VIEW
    if ($"{dataRow[3]}".Equals("VIEW"))
    {
        Console.WriteLine($"{dataRow[3]}");
        continue;
    }
    ..
    ..
}
```

Selects all the table data from the the indexed table within the loop

```
SqlCommand headerData = new SqlCommand($"SELECT * FROM {(string)dataRow[2]}", ClientDataBaseConnection);
SqlDataReader readerHeader = headerData.ExecuteReader();
```

Creates a new connection to the END databse and deletes the data in the selected table

```
using (SqlConnection EndDataBaseConnection = new SqlConnection(endDataBase))
{
    EndDataBaseConnection.Open()
    SqlCommand deleteHeader = new SqlCommand($"DELETE FROM {(string)dataRow[2]}", EndDataBaseConnection);
    var numberOfDeletedRows = deleteHeader.ExecuteNonQuery();
    ..
    ..
}
```

finally the data is copyed from the client database to the end database
(readerHeader is the orginal select from the source database)

```
using (SqlBulkCopy bulkCopy = new SqlBulkCopy(endDataBase))
{
    bulkCopy.DestinationTableName = (string)dataRow[2];
    try
    {
        bulkCopy.WriteToServer(readerHeader);
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
    finally
    {
        readerHeader.Close();
    }
}
```
